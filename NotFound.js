import Banner from '../components/Banner';
import { Jumbotron,Button, Row, Container} from 'react-bootstrap'
import React from 'react'

export default function NotFound(){

	let bannerContent = {
		title : 'PAGE CANNOT BE FOUND2',
		description : 'page does not exist!',
		buttonCallToAction : 'Back to Home'
		// destination: "/"
	}

	return(

		<Banner bannerProp = {bannerContent}/>
	
	)
	
}